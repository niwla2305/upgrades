upgrades = {}

function upgrades.register_upgrade(itemname, description, overlay, physics_override)
    minetest.register_craftitem("upgrades:"..itemname, {
        description = description,
        inventory_image = "default_paper.png^"..overlay,
        on_use = function(itemstack, user, pointed_thing)
            user:set_physics_override(physics_override)
            itemstack:take_item()
            local playername = user:get_player_name()
            minetest.after(30, function()
                local player = minetest.get_player_by_name(playername)
                if player then
                    player:set_physics_override({speed=1,jump=1,gravity=1})
                end
            end)
            return itemstack
        end,
    })
end

upgrades.register_upgrade("speed", "Speed Upgrade", "upgrades_red.png", {speed=2})
upgrades.register_upgrade("extremejump", "Extreme Jump Upgrade", "upgrades_pink.png", {jump=2})
upgrades.register_upgrade("jump", "Jump Upgrade", "upgrades_yellow.png", {jump=1.25})
upgrades.register_upgrade("lowgravity", "Low Gravity Upgrade", "upgrades_green.png", {gravity=0.5})
upgrades.register_upgrade("nogravity", "No Gravity Upgrade", "upgrades_green.png", {gravity=0.01})


minetest.register_chatcommand("resetupgrades", {
	params = "<player>",
	description = "Test 1: Modify player's inventory view",
    func = function(name, param)
        local player = minetest.get_player_by_name(param)
        player:set_physics_override({gravity=1, speed=1, jump=1})
	end,
})